use airport;

#Homework tasks group 6 (Конкатенація стрічок чи мат. обчислення чи робота з датами) 
#Task БД «Аеропорт». Для таблиці Pass_in_trip значення стовпця place розбити на два стовпця з коментарями, наприклад, перший – 'ряд: 2' та другий – 'місце: a'.
select trip_no , date, ID_psg, 
	regexp_substr(pass_in_trip.place, '[0-9]+') as 'ряд', 
	regexp_substr(pass_in_trip.place, '[^0-9]{1}') as 'місце'
from pass_in_trip;

#Homework tasks group 6 (Конкатенація стрічок чи мат. обчислення чи робота з датами)
#Task 6. БД «Аеропорт». Вивести дані для таблиці Trip з об’єднаними значеннями двох стовпців: town_from та town_to, з додатковими коментарями типу: 'from Rostov to Paris'.
select trip_no, ID_comp, plane, concat('from ',town_from,' to ', town_to) as from_to, time_out, time_in from trip;