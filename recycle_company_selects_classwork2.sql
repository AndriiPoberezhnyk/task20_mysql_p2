use recycle_company;

#Classwork task 7
select income_o.point, income_o.date , income_o.inc,
	case 
		when outcome_o.date = income_o.date then outcome_o.out
        else 0
    end as 'out'
from income_o
left join outcome_o on income_o.point = outcome_o.point and income_o.date = outcome_o.date
union 
select  daysWithoutInc.point, daysWithoutInc.date, 0 as 'inc', daysWithoutInc.out 
from (select * from outcome_o where outcome_o.date != all(select date from income_o)) as daysWithoutInc 
right join income_o on income_o.date != daysWithoutInc.date;
