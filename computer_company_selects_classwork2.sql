use computer_company;

#Classwork task 1
select distinct maker from product where maker = any (select maker from product where type ='pc') and maker = any (select maker from product where type = 'laptop');

#Classwork task 2
select distinct maker from product where maker = any (select maker from product where type ='pc') and maker != all (select maker from product where type = 'laptop');

#Classwork task 3
select maker as product_maker, 
	(select count(*) from pc, product p where p.maker = product_maker and pc.model =p.model) as pc,
	(select count(*) from laptop, product p where p.maker = product_maker and laptop.model =p.model) as laptop,
	(select count(*) from printer, product p where p.maker = product_maker and printer.model =p.model) as printer
from product group by maker having maker = product.maker;

#Classwork task 4
select  product.maker, min(printer.price) as price from printer, product where printer.model = product.model;

#Classwork task 5
select product.* from product 
where product.maker = any (select maker from product where type ='printer') 
	and maker = any (select maker from product where type = 'pc') 
		and model = any(select model from pc where ram = (select min(ram) from pc));
	
#Classwork task 6
select avg(makerA.price) as 'avg(price)' from 
(select product.maker, products.* 
	from product right join (select model, price from pc union all select model, price from laptop) as products 
		on product.model = products.model 
		where product.maker ='A') as makerA;	
