use computer_company;

#Homework tasks group 4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#Task 1
select pc_makers.maker from 
(select distinct product.maker, pc.model from pc, product where product.model = pc.model) as pc_makers
where pc_makers.maker not in (select distinct product.maker from laptop, product where product.model = laptop.model);

#Homework tasks group 4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#Task 2
select pc_makers.maker from 
(select distinct product.maker, pc.model from pc, product where product.model = pc.model) as pc_makers
where pc_makers.maker != all (select distinct product.maker from laptop, product where product.model = laptop.model);

#Homework tasks group 4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#Task 3 ?? why cant we write  '!= any' instead of 'not (.. = any ..)'
select pc_makers.maker 
from (select distinct product.maker, pc.model from pc, product where product.model = pc.model) as pc_makers
where not (pc_makers.maker = any (select distinct product.maker from laptop, product where product.model = laptop.model));


#Homework tasks group 4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#Task 4
select pc_makers.maker 
from (select distinct maker from product where product.type = 'pc') as pc_makers
where pc_makers.maker in (select distinct maker from product where product.type = 'laptop');

#Homework tasks group 4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#Task 5
select pc_makers.maker from 
(select distinct maker from product where product.type = 'pc') as pc_makers
where not (pc_makers.maker != all (select distinct maker from product where product.type = 'laptop'));

#Homework tasks group 4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#Task 6 
select pc_makers.maker 
from (select distinct maker from product where product.type = 'pc') as pc_makers
where pc_makers.maker = any (select distinct maker from product where product.type = 'laptop');

#Homework tasks group 5 (Використання підзапитів з лог. операцією EXISTS)
#Task 1 БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК яких є у наявності в таблиці PC (використовуючи операцію EXISTS). Вивести maker.
select distinct maker 
from product where type= 'pc' 
and maker not in (select maker from product where type ='pc' and not exists (select model from pc where product.model = pc.model));

#Homework tasks group 5 (Використання підзапитів з лог. операцією EXISTS)
#Task 2. БД «Комп. фірма». Знайдіть виробників, які б випускали ПК зі швидкістю 750 МГц та вище. Виведіть: maker.
select maker
from product 
where exists (select model from pc where product.model = pc.model and pc.speed >=750);

#Homework tasks group 5 (Використання підзапитів з лог. операцією EXISTS)
#Task 3 БД «Комп. фірма». Знайдіть виробників, які б випускали одночасно ПК та ноутбуки зі швидкістю 750 МГц та вище. Виведіть: maker.
select pc_makers.maker 
from (select * from product where exists (select model from pc where product.model = pc.model and pc.speed >=750)) as pc_makers
where pc_makers.maker = some (select maker from product where exists (select model from laptop where product.model = laptop.model and laptop.speed >=750));

#Homework tasks group 5 (Використання підзапитів з лог. операцією EXISTS)
#Task 4 БД «Комп. фірма». Знайдіть виробників принтерів, що випускають ПК з найвищою швидкістю процесора. Виведіть: maker.
select distinct printer_makers.maker from (select * from product where type='printer') as printer_makers 
where exists (select * from product, pc where pc.speed = (select max(speed) from pc) and printer_makers.maker = product.maker and product.model = pc.model);

#Homework tasks group 5 (Використання підзапитів з лог. операцією EXISTS)
#Task 7 БД «Комп. фірма». Виведіть тих виробників ноутбуків, які також випускають і принтери.
select distinct laptop_makers.maker from (select * from product where type = 'laptop') as laptop_makers
where exists (select * from (select * from product where type ='printer') as printer_makers where laptop_makers.maker = printer_makers.maker);

#Homework tasks group 6 (Конкатенація стрічок чи мат. обчислення чи робота з датами)
#Task 1. БД «Комп. фірма». Виведіть середню ціну ноутбуків з попереднім текстом 'середня ціна = '.
select avg(price) as 'середня ціня' from laptop;
#по моделях
select model, count(*), avg(price) from laptop group by model;

#Homework tasks group 6 (Конкатенація стрічок чи мат. обчислення чи робота з датами)
#Task 2. БД «Комп. фірма». Для таблиці PC вивести усю інформацію з коментарями у кожній комірці, наприклад, 'модель: 1121', 'ціна: 600,00' і т.д.
select concat("'code: ", pc.code, "'") as 'code', concat("'model: ", pc.model, "'") as 'model', concat("'speed: ", pc.speed, "'") as 'speed',
		concat("'ram: ", pc.ram, "'") as 'ram', concat("'hd: ", pc.hd, "'") as 'hd', concat("'cd: ", pc.cd, "'") as 'cd', concat("'price: ", pc.price, "'") as 'price' from pc;
 
#Homework tasks group 7 (Статистичні функції та робота з групами)
#Task 1. БД «Комп. фірма». Знайдіть принтери, що мають найвищу ціну. Вивести: model, price.
select * from printer where price = (select max(price) from printer);
 
#Homework tasks group 7 (Статистичні функції та робота з групами)
#Task 2. БД «Комп. фірма». Знайдіть ноутбуки, швидкість яких є меншою за швидкість будь-якого з ПК. Вивести: type, model, speed.
select product.type, laptop.model, laptop.speed 
from laptop,product 
where laptop.speed < all(select speed from pc) and product.model = laptop.model;
#another solution
select product.type, laptop.model, laptop.speed 
from laptop,product
where laptop.speed < (select min(speed) from pc) and product.model = laptop.model;
 
#Homework tasks group 7 (Статистичні функції та робота з групами)
#Task 3. БД «Комп. фірма». Знайдіть виробників найдешевших кольорових принтерів. Вивести: maker, price.
select product.maker, printer.price 
from product,printer 
where printer.price = (select min(price) from printer where printer.color='y') and product.model = printer.model and printer.color='y';
 
#Homework tasks group 7 (Статистичні функції та робота з групами)
#Task 4. БД «Комп. фірма». Знайдіть виробників, що випускають по крайній мірі дві різні моделі ПК. Вивести: maker, число моделей. (Підказка: використовувати підзапити у якості обчислювальних стовпців та операцію групування)
select distinct maker from product where exists(select *, count(*) models from pc where product.model = pc.model group by model having models>=2); 

#Homework tasks group 7 (Статистичні функції та робота з групами)
#Task 5. БД «Комп. фірма». Знайдіть середній розмір жорсткого диску ПК (одне значення на всіх) тих виробників, які також випускають і принтери. Вивести: середній розмір жорсткого диску.
#select distinct maker from product where type = 'printer'; 
select avg(hd) as avg_ram
from (select distinct product.maker, pc.* 
		from pc, product  
		where product.maker = any (select distinct maker from product where type = 'printer') 
		and product.model = pc.model) as pc_from_printer_makers;
    
#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 1. БД «Комп. фірма». Для таблиці Product отримати підсумковий набір у вигляді таблиці зі стовпцями maker, pc, laptop та printer, в якій для кожного виробника необхідно вказати кількість продукції, що ним випускається, тобто наявну загальну кількість продукції у таблицях, відповідно, PC, Laptop та Printer. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
#in classwork sql another solution
select maker, sum(stats.pc) as pc ,sum(stats.laptop) as laptop,sum(stats.printer) as printer
from (select maker, 
		(select count(*) from pc where product.model = pc.model and product.maker = maker) as pc,
		(select count(*) from laptop where product.model = laptop.model) as laptop,
		(select count(*) from printer where product.model = printer.model) as printer 
	 from product) as stats
group by maker;

#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 2. БД «Комп. фірма». Для кожного виробника знайдіть середній розмір екрану для ноутбуків, що ним випускається. Вивести: maker, середній розмір екрану. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
select maker , avg(laptops.screen) as size
from (select maker,
		(select avg(screen) from laptop where product.model = laptop.model and product.maker = maker) as screen
		from product) as laptops
group by maker having size is not null;

#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 3. БД «Комп. фірма». Знайдіть максимальну ціну ПК, що випускаються кожним виробником. Вивести: maker, максимальна ціна. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
select maker, max(max_price) as max_price #max price for each maker
from (select maker,
		(select max(price) from pc where product.model = pc.model and product.maker = maker) as max_price #max price for each model
		from product) as pcs
group by maker having max_price is not null;

#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 4. БД «Комп. фірма». Знайдіть мінімальну ціну ПК, що випускаються кожним виробником. Вивести: maker, максимальна ціна. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
select maker, min(min_price) as min_price #min price for each maker
from (select maker,
		(select min(price) from pc where product.model = pc.model and product.maker = maker) as min_price  #min price for each model
		from product) as pcs
group by maker having min_price is not null;

#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 5. БД «Комп. фірма». Для кожного значення швидкості ПК, що перевищує 600 МГц, визначіть середню ціну ПК з такою ж швидкістю. Вивести: speed, середня ціна. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
select speed, avg(price)
from pc
where speed > 600
group by speed;

#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 6. БД «Комп. фірма». Знайдіть середній розмір жорсткого диску ПК кожного з тих виробників, які випускають також і принтери. Вивести: maker, середній розмір жорсткого диску. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
select maker, avg(hd)
from (select product.maker, pc.*
		from pc, product
        where product.maker in (select distinct maker from product where type = 'printer') 
        and product.model = pc.model) as mks
group by maker;

#Homework tasks group 9 (Оператор CASE)
#Task 1. БД «Комп. фірма». Для таблиці Product отримати підсумковий набір у вигляді таблиці зі стовпцями maker, pc, в якій для кожного виробника необхідно вказати, чи виробляє він ('yes'), чи ні ('no') відповідний тип продукції. У першому випадку ('yes') додатково вказати поруч у круглих дужках загальну кількість наявної (тобто, що знаходиться у таблиці PC) продукції, наприклад, 'yes(2)'. (Підказка: використовувати підзапити у якості обчислювальних стовпців та оператор CASE)
select product.maker,
	case 
		when product.maker = pc_maker.maker then concat('yes(', pc_maker.amount,')')
        when product.maker not in (select distinct maker from product where type ='pc')   then 'no'
        else null
	end as make_pc
from (select product.maker, count(*) as amount  
            from product,pc 
            where pc.model = product.model
			group by product.maker) as pc_maker, product
group by product.maker, make_pc having make_pc is not null;    

#Homework tasks group 10 (Об’єднання UNION)
#Task 1. БД «Комп. фірма». Знайдіть номера моделей та ціни усіх продуктів (будь-якого типу), що випущені виробником 'B'. Вивести: maker, model, type, price. (Підказка: використовувати оператор UNION)
select product.maker, all_in_one.model, all_in_one.price
from (select pc.model , pc.price 
	from pc
	union all
	select laptop.model , laptop.price 
	from laptop
	union all
	select  printer.model , printer.price 
	from printer) as all_in_one, product
where product.maker = 'B' and product.model = all_in_one.model;

#Homework tasks group 10 (Об’єднання UNION)
#Task 2. БД «Комп. фірма». Для кожної моделі продукції з усієї БД виведіть її найвищу ціну. Вивести: type, model, максимальна ціна. (Підказка: використовувати оператор UNION)
select product.type, all_in_one.model, max(all_in_one.price) as max_price
from (select pc.model , pc.price 
	from pc
	union all
	select laptop.model , laptop.price 
	from laptop
	union all
	select  printer.model , printer.price 
	from printer) as all_in_one, product
where product.model = all_in_one.model
group by model;

#Homework tasks group 10 (Об’єднання UNION)
#Task 3. БД «Комп. фірма». Знайдіть середню ціну ПК та ноутбуків, що випущені виробником 'A'. Вивести: одна загальна середня ціна. (Підказка: використовувати оператор UNION)
select product.maker, avg(all_in_one.price) as avg_price_pc_and_laptop
from (select pc.model , pc.price 
	from pc
	union all
	select laptop.model , laptop.price 
	from laptop
	union all
	select  printer.model , printer.price 
	from printer) as all_in_one, product
where product.model = all_in_one.model and (product.type ='pc' or product.type ='laptop')
group by maker;


