use ships;

#Homework tasks group 10 (Об’єднання UNION)
#Task 4. БД «Кораблі». Перерахуйте назви головних кораблів, що є наявними у БД (врахувати також і кораблі з таблиці Outcomes). Вивести: назва корабля, class. (Підказка: використовувати оператор UNION та операцію EXISTS)
select distinct each_ship.name, 
	case 
		when not exists(select * from ships where each_ship.name = ships.name)  then 'unknown'
        else (select class from ships where each_ship.name = ships.name)
	end as class
from (select name
	from ships
	union 
	select ship
	from outcomes) as each_ship;
    
#Homework tasks group 10 (Об’єднання UNION)
#Task 5. БД «Кораблі». Знайдіть класи, у яких входить лише один корабель з усієї БД (врахувати також кораблі у таблиці Outcomes, яких немає у таблиці Ships). Вивести: class. (Підказка: використовувати оператор UNION та операцію EXISTS)
select class
from(select distinct each_ship.name, 
		case 
			when not exists(select * from ships where each_ship.name = ships.name)  then each_ship.name # if class not found -> class_name => ship_name
			else (select class from ships where each_ship.name = ships.name)
		end as class
	from (select name
		from ships
		union 
		select ship
		from outcomes) as each_ship) as ships
group by class having count(*) = 1;    